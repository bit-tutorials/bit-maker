# Bit-Tutorial for Vue

## Project setup
```
vue create project-name
```

## Create a Free bit.dev Account

## make a component in your project

## Install Bit CLI
```
Install Bit CLI on your computer using npm:
npm install bit-bin -g

If you have Bit installed, verify the installation by running the command:
bit --version
```

## Login to Your Bit Account
```
Authenticate Bit to your bit.dev account. From the command line run:
bit login
```

## Initialize Bit Workspace

```
Switch to the Vue tutorial project directory and run the Bit initialization command:

$ bit init
successfully initialized a bit workspace.

Notice two other changes have happened:

:- A new file named .bitmap has been created in your root directory. This file tracks Bit components and only includes a comment and a line with your bit version.

:- A new section, bit, has been added to your package.json file with the following defaults for your project:
```
## Track a New Component
```
To track the Hello-World component, we will need to tell Bit about the component and the files that are related to it. In Vue, a component is typically a single file so we can directly add this file.
Command:
bit add src/components/Hello-World.vue
```

## Install Vue Compiler
```
So far, we have provided Bit with the source file of the component. But in order to consume the files in other projects, the component needs to be built.
To build the vue component, you'll need the Vue compiler. Install the compiler and run this command inside the Vue tutorial repository:
    $ bit import bit.envs/compilers/vue --compiler
    the following component environments were installed
    - bit.envs/compilers/vue@0.0.7

The Vue compiler is now set as the default compiler for the Bit workspace inside this repository.
```

## Build the Vue Component
```
Now that the compiler is installed, build the component. Building the component serves two purposes:

Make the component directly consumable by other projects.
Make sure that the component is all-inclusive and contains all the parts that are required in order to share it with others.

run the following command:
$ bit build
```

## Export Component
```
With the component properly built, it is now time to share it with the world.
Components are versioned according to semver standards. To tag your component with a version, run the following command:

$ bit tag --all 0.0.1
1 component(s) tagged
(use "bit export [collection]" to push these components to a remote")
(use "bit untag" to unstage versions)

new components
(first version for components)
     > product-list@0.0.1

This command tags all the components that are currently staged in Bit. In our case, it's only the product-list component.



You can check the component status (bit status) and you'll find the following:

$ bit status
staged components
(use "bit export <remote_scope> to push these components to a remote scope")

     > product-list. versions: 0.0.1 ... ok


To export the component to your bit.dev collection, we will use the export command and the full name of the collection, structured as <username>.<collection>:

example:
$ bit export <username>.vue-tutorial
exported 1 components to scope <username>.vue-tutorial
```
## Preview the Vue Component
```
The Vue component is also available on the bit.dev cloud. Go to https://bit.dev and log into your account (if you are not logged in yet):

:-Select the collections navigator on the left panel and select collections.
:-Click on your collection--you׳ll see your hello-world component.
:-Click on the hello-world component to see its playground.
```
# Install Component in Another Project

## Create a New Vue Application

```
If you already have vue-cli installed globally you can run:

vue create my-new-vue

Make sure you are using babel and es6.

In your terminal, switch to the my-new-app directory.
```

## Install the Component in Your Project
```
The component is stored in the Bit registry, so the full path to the component will be: @bit/<username>.<collection name>.<component name>

Run the install command using npm:

npm install @bit/<username>.vue-tutorial.product-list --save

The component is now added to your package.json:

"@bit/<username>.vue-tutorial.product-list": "0.0.1"
```

## Use In Your Application
```
Now you can use the component in your code, just like any other import. Your app component should look like this:

<template>
  <div id="app">
    <hello-world />
  </div>
</template>

<script>
import HelloWorld from '@bit/<username>.vue-tutorial.hello-world';

export default {
  name: 'app',
  components: {
    'hello-world': HelloWorld
  }
}
</script>
```

# Modify the Component
```
Next, we are going to make a change to the component and export it back to the collection.
```

## Import the Component
```
Up until now, the hello-world component was only installed (in its built form) in our project. Now, we want to import the code into our project to make the changes.

In order to import the component, initiate the my-new-app workspace as a Bit workspace:

bit init


After the confirmation message that the workspace was initialized, run the following command:

$ bit import <username>.vue-tutorial/hello-world
successfully imported one component
- added <username>.vue-tutorial/hello-world new versions: 0.0.1, currently used version 0.0.1

The command is also available on the component page.
Here is what happened:

A new top-level components folder is created that includes the code of the component, with its compiled code and node_modules (in this case the node_modules are empty, as all of your node_modules are peer dependencies and are taken from the root project.
The .bitmap file was modified to include the reference to the component
The package.json file is modified to point to the files rather than the remote package. Your package.json now displays:
{
  "@bit/<username>.vue-tutorial.product-list": "file:./components/product-list"
}
```

## Update the Code
```
The app is not yet changed. That's because the Bit components are compiled by the bit compiler. In a separate terminal, run the bit build command to compile the changes. You should see that the compiler is installed:

successfully installed the bit.envs/compilers/Vue@0.0.7 compiler

That will be followed by a successful compilation of the main file.

Run the my-new-app again and you'll now see the changed component.
```

## Export the Changes
```
Next, export the changes done to the component back to bit.dev.
command:
bit status

The product-list component was modified:

modified components
(use "bit tag --all [version]" to lock a version with all your changes)
(use "bit diff" to compare changes)

     > product-list ... ok


Tag and export the component as a new version. By default this is a semver patch version:

$ bit tag hello-world
1 component(s) tagged
(use "bit export [collection]" to push these components to a remote")
(use "bit untag" to unstage versions)

changed components
(components that got a version bump)
     > <username>.vue-tutorial/hello-world@0.0.2


Export it back to the collection:

$ bit export <username>.vue-tutorial
exported 1 components to scope <username>.vue-tutorial

Head to the component page on bit.dev. Here you can see that the component has a new version. The changes are also visible on the component playground.
```

# Get Component Updates

## Import Changes
```
Run bit import to see if any components were changed (similar to doing git pull to check git changes).

We will see that the hello-world component was changed and a new version exists:

$ bit import
successfully imported one component
- updated <username>.vue-tutorial/hello-world new versions: 0.0.2


The component is downloaded but is not yet changed. Check the workspace status, you will get the following:

$ bit status
pending updates
(use "bit checkout [version] [component_id]" to merge changes)
(use "bit diff [component_id] [new_version]" to compare changes)
(use "bit log [component_id]" to list all available versions)

    > <username>.vue-tutorial/hello-world current: 0.0.1 latest: 0.0.2
```

## Checkout
```
Merge the changes done to the component to your project. The structure of the command is bit checkout <version> <component>. So you run:

$ bit checkout 0.0.2 hello-world
successfully switched <username>.vue-tutorial/hello-world to version 0.0.2
  

Bit performs a git merge. The code from the updated component is now merged into your code.
```